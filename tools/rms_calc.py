import sys
sys.path.append('/local/scratch/harrison/ext/casa/analysis_scripts/')

import numpy as np
import analysisUtils as au

import pdb

if __name__=='__main__':
  
  #msname = '/scratch/nas_mberc2/SuperCLASS/raw/SuperMap/1026+6812_26072015.ms'
  #msname = '/scratch/nas_mberc2/SuperCLASS/raw/SuperMap/1022+6812_30122014.ms'
  #msname = '/local/scratch/harrison/simuCLASS/allsource-realcoverage-e_merlin-ws112.ms'
  #msname = '/local/scratch/harrison/simuCLASS/emerlin-firstlight.ms'  
  #msname = '/scratch/nas_mberc2/SuperCLASS/raw/SuperMap/1022+6800_SelfCal.ms/'
  msname = '/scratch/nas_mberc2/SuperCLASS/harrison/jvla-epochs/epoch1/M27_e1.ms'

  
  ms.open(msname)
  
  baselines = au.getBaselineLengths(msname, sort=True)
  
  pdb.set_trace()
  
  #baselines = np.asarray(baselines[2:22])[:,0]
  baselines = np.asarray(baselines)[:,0]
  
  for i in np.arange(baselines.shape[0]):
    if baselines[i].startswith('-'):
      continue  
    else:
      baselines[i] = baselines[i].replace('-', '&')
  
  spws = np.arange(1,8)
  spws = np.asarray(spws, dtype=str)
  
  rmsvals = {}
  rmsvals_arr = []
  
  for sp in spws:
    for bl in baselines:
      if bl.startswith('-'):
        continue
      else:
        print(bl)
        try:
          stats = ms.statistics(column='DATA', baseline=bl, spw=sp, complex_value='real')
        except:
          continue
        
        rmsvals_arr.append(stats['DATA']['rms'])
        
        rmsvals[bl, sp] = stats['DATA']['rms']
