def nrows_per_baseline(vis, scn):
  blinelist = [('0', '6'),
               ('0', '7'),
               ('1', '5'),
               ('1', '4'),
               ('5', '6'),
               ('4', '5'),
               ('0', '4'),
               ('1', '8'),
               ('6', '7'),
               ('4', '8'),
               ('1', '7'),
               ('1', '6'),
               ('0', '8'),
               ('7', '8'),
               ('6', '8'),
               ('5', '8'),
               ('5', '7'),
               ('4', '6'),
               ('4', '7'),
               ('0', '5'),
               ('0', '0'),
               ('1', '1'),
               ('4', '4'),
               ('5', '5'),
               ('6', '6'),
               ('7', '7'),
               ('8', '8'),
               ]

  nrows = []
  ms.open(vis)             
  for bline in blinelist:
    ms.selectinit(datadescid=0)
    ms.select({'antenna1' : int(bline[0]), 'antenna2' : int(bline[1])})
    #ms.select({'scan_number' : int(scn)})
    ms.select({'scan_number' : [1,2,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,30,31,32]})
    #ms.select({'time' : [4.92677002e+09, 4.96625160e+09]})
    #ms.select({'time' : [4.94678002e+09, 4.96625160e+09]})
    ant1 = ms.getdata('antenna1')
    print(bline, len(ant1['antenna1']))
    nrows.append(len(ant1['antenna1']))
